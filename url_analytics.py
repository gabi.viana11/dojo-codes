#! /usr/bin/env python
# -*- coding: utf-8 -*-

import re


class AnalisadorURL:

    url = ""

    def obterUrl(self):
        while self.url == '':
            self.url = input("Insira a URL a ser verificada para que possamos prosseguir: ")

    def validaURL(self):
        if self.url != '':
            validate = re.search("(http|https|ssh)\:\/{2}(w{3}|[a-zA-Z]*)\%*([a-zA-z0-9]*)\@*\.*([a-zA-z0-9]*\.com|gov|net)/*([a-zA-z0-9]*)/*([a-zA-z]*\=[a-zA-Z0-9]*)*",self.url)
            if validate != None:
                if validate.group(1) == 'http':
                    print("Protocolo: {} \nHost: {} \nDomínio: {}\nPath: {}\nParâmetros: {}".format(validate.group(1), validate.group(2), validate.group(4), validate.group(5), validate.group(6)))
                elif validate.group(1) == 'ssh':
                    print("Protocolo: {} \nUsuário: {} \nSenha: {}\nDomínio: {}".format(validate.group(1), validate.group(2), validate.group(3), validate.group(4)))
                else:
                    print("Erro na classificação da url")

            else:
                print("Oops! Você inseriu uma URL inválida!")


a = AnalisadorURL()
a.obterUrl()
a.validaURL()