# dojo-codes

Projeto inicial do meu repositório onde serão inseridos scripts de estudo na linguagem Python contendo resoluções de exercícios de programação propostos no Dojo Puzzles (http://dojopuzzles.com)

## Começando

Essas instruções farão com que você tenha uma cópia do projeto em execução na sua máquina local para fins de desenvolvimento e teste. Veja as notas sobre como baixar os arquivos

### Prerequisitos

Além de ter o git instalado na sua máquina, você não precisará de mais nada por enquanto :)

Você pode clonar o repositório ou baixá-lo zipado em sua pasta de projetos:
```
 git clone url
```

## Autores

* **Gabriela Cristina** - *Initial work* 

## Licença

Este projeto está licenciado sob a licença MIT